
import { AntdSkMenu }  from '../../sk-menu-antd/src/antd-sk-menu.js';

export class Antd4SkMenu extends AntdSkMenu {

    get prefix() {
        return 'antd4';
    }

}
